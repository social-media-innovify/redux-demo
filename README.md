#REDUX NOTES

- Redux is library for JS applications to manage state in predictable way.
- Redux can be used with React, Angular, Vue and also with Vanilla JavaScript.

==> Install:-

    Run 'npm init --yes' to create package.json file.
    Run 'npm install redux' and it will install redux and add dependency to package.json file

==> principle:-

    1. State of whole application is stored in an object tree within a single store
    eg.
    {
        numberOfCakes: 10
    }

    2. Can't update state directly. We can do it with an action
    eg. If action is buying a cake
    {
        type: BUY_CAKE
    }

==> Action:-

    - Describe that what changes the application.
    eg.
    const BUY_CAKE='BUY_CAKE'
    function buyCake(){
        Return {
            type: BUY_CAKE,
            info: 'First redux action'
        }
    }

==> Reducers:-

    - Describe how application state changes.
    (prevState, action) => newState

    eg.
    const initState = {
        numOfCakes: 10
    }

    const reducer = (state = initState, action) => {
        switch(action.type) {
            case BUY_CAKE: return {
                ...state,       //copy all other state values
                numberOfCakes: state.numOfCakes - 1
            }
        }
    })

==> Redux store:-

    import redux to js
    eg. import Redux from 'redux'   //If node module is install
    const Redux = require('redux')  //with plain js
    const createStore = redux.createStore;

    const store = createStore(reducer);     //You can only pass one reducer
    console.log(store.getState())           //store will have method called getState(),
                                            // which returns state data.

==> Multiple Reducers:-

    We should create more than one reducers saperately as functionality.

    eg. const initCakeState = {
        type: BUY_CAKE
    }

    const initIceCreamState = {
        type: BUY_ICECREAM
    }

    const cakeReducer = (state = initCakeState, action) => {
        switch (action.type) {
            case BUY_CAKE:
                return {
                    ...state,
                    numOfCakes: state.numOfCakes - 1
                };
            default:
                return state;
        }
    };

    const iceCreamReducer = (state = initIceCreamState, action) => {
        switch (action.type) {
            case BUY_ICECREAM:
                return {
                    ...state,
                    numOfIceCreams: state.numOfIceCreams - 1
                };
            default:
                return state;
        }
    };

    But can not pass multiple reducers in createStore() function, as shown above.
    But we can combine reducers as follow.

==> Combine Reducers:-

    Combined multiple reducers as one object.

    const combinedReducers = redux.combinedReducers;
    const rootReducer = combinedReducers({
        cake: cakeReducer,
        iceCream: iceCreamReducer
    })

    const store = createStore(rootReducer);

==> Middleware:-

    - Middleware is use to extend redux with custom functionality.
    - It provides third-party extension points b/w dispatching action, and moment it reaches reducer
    - Use middleware for logging, crash reporting, performing async tasks, etc.

    Let's take example of redux-logger middleware.
    eg.
    Run 'npm install redux-logger'

    const reduxLogger = require('redux-logger');
    const logger = reduxLogger.createLogger();

    const applyMiddleware = redux.applyMiddleware;

    const store = createStore(rootReducer, applyMiddleware(logger));
    // Pass the applyMiddleware() func. as second parameter of store and pass middleware we are incorporating in applyMiddleware(logger) function as parameter. (Here we used logger).
    - It will log the data whenever the state of store is changed automatically.

==> Async Actions and Thunk Middleware:-

    - In redux if we are working on any async actions, we have to use any middleware to perform async action.
    - Redux Thunk is use for using async actions in redux. (api call, file read write, etc)
    eg.
    Shown in asyncAction.js file
